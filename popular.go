package robinhood

import (
	"encoding/json"
	"fmt"
	"net/url"
)

type PopularItems struct {
	CanonicalExamples string   `json:"canonical_examples"`
	Description       string   `json:"description"`
	Instruments       []string `json:"instruments"`
	Name              string   `json:"name"`
	Slug              string   `json:"slug"`
}

type EnhancedPopularItem struct {
	Symbol string
	Name   string
}

// Portfolio returns a slice of Position a user has in their account.
func (c *Client) Popular() ([]EnhancedPopularItem, error) {
	var enhancedPopularItems []EnhancedPopularItem
	pi, err := c.popular()
	if err != nil {
		return nil, err
	}
	for _, p := range pi.Instruments {
		detail, err := getInstrument(p)
		if err != nil {
			continue
		}
		enhancedPopularItems = append(enhancedPopularItems, EnhancedPopularItem{
			Symbol: detail.Symbol,
			Name:   detail.Name,
		})
	}
	return enhancedPopularItems, nil
}

func (c *Client) popular() (PopularItems, error) {
	params := url.Values{}
	params.Set("nonzero", "true")
	resp, err := c.get(popular100URI)
	if err != nil {
		return PopularItems{}, err
	}
	var popularItems PopularItems

	fmt.Println(popularItems)
	err = json.Unmarshal(resp, &popularItems)
	if err != nil {
		return PopularItems{}, err
	}
	return popularItems, nil
}
