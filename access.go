// Package robinhood is a client of Robinhood's trading and screening API.
package robinhood

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	apiURL           = "https://api.robinhood.com/"
	tokenURI         = "api-token-auth/"
	accountsURI      = "accounts/"
	positionsURI     = "positions/"
	quotesURI        = "quotes/"
	chainsURI        = "options/chains/"      // ?equity_instrument_ids=
	optionsURI       = "options/instruments/" //?chain_id={_chainid}&expiration_dates={_dates}&state=active&tradability=tradable
	marketOptionsURI = "marketdata/options/"  //{_optionid}/
	oAuthURI         = "oauth2/token/"
	ordersURI        = "orders/" //{_orderId}/
	watchlistsURI    = "watchlists/"
	popular100URI    = "midlands/tags/tag/100-most-popular/"
	dividendsURI     = "dividends/"
	transfersURI     = "ach/transfers/"
)

// get performs an HTTP get request on 'endpoint'..
func (c *Client) get(endpoint string) ([]byte, error) {
	req, err := http.NewRequest("GET", apiURL+endpoint, nil)
	if err != nil {
		return nil, err
	}
	return c.doReqWithBearerToken(req)
}

// post performs an HTTP post of 'data' to 'endpoint'. Data is URL-encoded, not JSON.
func (c *Client) post(endpoint string, data string) ([]byte, error) {
	buf := strings.NewReader(data)
	req, err := http.NewRequest("POST", apiURL+endpoint, buf)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	// Note: Content-Length is set by NewRequest.
	return c.doReqWithAuth(req)
}

// post performs an HTTP post of 'data' to 'endpoint'. Data is not JSON.
func (c *Client) postJson(endpoint string, data string) ([]byte, error) {
	buf := strings.NewReader(data)
	req, err := http.NewRequest("POST", apiURL+endpoint, buf)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	// Note: Content-Length is set by NewRequest.
	return c.doReqWithBearerToken(req)
}

// post performs an HTTP post of 'data' to 'endpoint'. Data is not JSON.
func (c *Client) postJsonNoAuth(endpoint string, data string) ([]byte, error) {
	buf := strings.NewReader(data)
	req, err := http.NewRequest("POST", apiURL+endpoint, buf)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	// Note: Content-Length is set by NewRequest.
	return c.doReqWithAuth(req)
}

func (c *Client) doReqWithAuth(req *http.Request) ([]byte, error) {
	_, hasAuthorizationHeader := req.Header["Authorization"]
	if c.Token != "" && !hasAuthorizationHeader {
		req.Header.Add("Authorization", "Token "+c.Token)
	}
	return DoReq(req)
}

func DoReq(req *http.Request) ([]byte, error) {
	req.Header.Add("Accept", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return data, fmt.Errorf("Request failed: %s (%d): %s", resp.Status, resp.StatusCode, data)
	}
	return data, nil
}

func (c *Client) doReqWithBearerToken(req *http.Request) ([]byte, error) {
	err := c.EnsureBearerToken()
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Bearer "+c.BearerToken)
	return DoReq(req)
}
