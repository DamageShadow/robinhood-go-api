package robinhood

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

// Instrument represents a Robinhood resource.
type Instrument string

// GetID returns the ID part of the Instrument.
func (i Instrument) GetID() string {
	fields := strings.Split(string(i), "/")
	if len(fields) < 2 {
		return ""
	}
	return fields[len(fields)-2]
}

type detailedPosition struct {
	Symbol string `json:"symbol"`
	Name   string `json:"simple_name"`
}

func getInstrument(url string) (detailedPosition, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Errorf("Error fetching details for position %s", err.Error())
		return detailedPosition{}, err
	}
	resp, err := DoReq(req)
	if err != nil {
		fmt.Errorf("Error fetching details for position %v", err)
	}

	var detail detailedPosition
	err = json.Unmarshal(resp, &detail)
	if err != nil {
		fmt.Errorf("Error unmarshalling details for position %v", err)
	}
	return detail, nil
}
