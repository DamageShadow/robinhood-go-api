package robinhood

import (
	"encoding/json"
	"net/url"
)

// This file deals with portfolio information for a given account.

// Position identifies a position in a portfolio.
type Position struct {
	Symbol   string
	Name     string
	BuyPrice float64 `json:"average_buy_price,string"`
	Quantity float64 `json:"quantity,string"`
	// TODO: add other fields.
}

// Portfolio returns a slice of Position a user has in their account.
func (c *Client) Portfolio() ([]Position, error) {
	var positions []Position
	pos, err := c.portfolio()
	if err != nil {
		return nil, err
	}
	for _, p := range pos {

		detail, err := getInstrument(p.URL)
		if err != nil {
			continue
		}

		positions = append(positions, Position{
			Symbol:   detail.Symbol,
			Name:     detail.Name,
			BuyPrice: p.BuyPrice,
			Quantity: p.Quantity,
		})
	}
	return positions, nil
}

type position struct {
	BuyPrice float64 `json:"average_buy_price,string"`
	URL      string  `json:"instrument"`
	Quantity float64 `json:"quantity,string"`
}

func (c *Client) portfolio() ([]position, error) {
	parms := url.Values{}
	parms.Set("nonzero", "true")
	resp, err := c.paginatedGet(accountsURI + c.AccountID + "/" + positionsURI + "?" + parms.Encode())
	if err != nil {
		return nil, err
	}
	var positions []position
	err = json.Unmarshal(resp, &positions)
	if err != nil {
		return nil, err
	}
	return positions, nil
}
