package sentiments

import (
	"fmt"
	"github.com/cdipaolo/sentiment"
)

func main() {

	model, err := sentiment.Restore()
	if err != nil {
		panic(fmt.Sprintf("Could not restore model!\n\t%v\n", err))
	}
	sentiment.Train()
	// get sentiment analysis summary
	// in any implemented language
	analysis := model.SentimentAnalysis("I don't like your tone right now", sentiment.English) // 0

	sentiment.PersistToFile(model, "/Users/damage/go/src/robinhood/sentiments/model.json")

	fmt.Println(analysis.Score)
}
