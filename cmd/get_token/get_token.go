// get_token is a simple utility to get a user's token, given their username and password.
package main

import (
	"fmt"

	rh "robinhood"
)

func main() {

	client := rh.NewClient()

	fmt.Println(client.Token)
}
