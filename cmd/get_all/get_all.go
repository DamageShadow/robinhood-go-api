// get_token is a simple utility to get a user's token, given their username and password.
package main

import (
	"fmt"

	rh "robinhood"
)

func main() {

	client := rh.NewClient()

	accs, err := client.GetAccounts()
	if err != nil {
		panic(err)
	}
	fmt.Println("Accounts:")
	for _, acc := range accs {
		fmt.Printf("Account: %v\n", acc.AccountNumber)
		client.AccountID = acc.AccountNumber
	}

	portfolio, err := client.Portfolio()
	if err != nil {
		panic(err)
	}
	fmt.Println("Portfolio:")
	fmt.Println(portfolio)

	watchlist, err := client.WatchList()
	if err != nil {
		panic(err)
	}
	fmt.Println("WatchList:")
	fmt.Println(watchlist)

	popular, err := client.Popular()
	if err != nil {
		panic(err)
	}
	fmt.Println("Popular:")
	fmt.Println(popular)

	dividends, err := client.Dividends()
	if err != nil {
		panic(err)
	}
	fmt.Println("Transfer:")
	fmt.Println(dividends)

}
