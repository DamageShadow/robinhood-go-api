// get_token is a simple utility to get a user's token, given their username and password.
package main

import (
	"fmt"
	"strings"

	rh "robinhood"
	"strconv"
)

func main() {

	client := rh.NewClient()

	accs, err := client.GetAccounts()
	if err != nil {
		panic(err)
	}
	fmt.Println("Accounts:")
	for _, acc := range accs {
		fmt.Printf("Account: %v\n", acc.AccountNumber)
		client.AccountID = acc.AccountNumber
	}

	dividends, err := client.Dividends()
	if err != nil {
		panic(err)
	}
	fmt.Println("Transfer:")
	fmt.Println(dividends)

	total := 0.

	for _, dividend := range dividends {
		f, err := strconv.ParseFloat(dividend.Amount, 64)
		if err != nil {
			panic(err)
		}
		if strings.HasPrefix(dividend.PayableDate, "") {
			total = total + f
		}

	}

	fmt.Println("Transfer Total:")
	fmt.Println(total)

}
