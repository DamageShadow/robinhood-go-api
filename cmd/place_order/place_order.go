// get_token is a simple utility to get a user's token, given their username and password.
package main

import (
	"fmt"

	rh "robinhood"
)

func main() {

	client := rh.NewClient()

	accs, err := client.GetAccounts()
	if err != nil {
		panic(err)
	}
	fmt.Println("Accounts:")
	for _, acc := range accs {
		fmt.Printf("Account: %v\n", acc.AccountNumber)
		client.AccountID = acc.AccountNumber
	}

}
