// get_token is a simple utility to get a user's token, given their username and password.
package main

import (
	"fmt"
	"strings"

	rh "robinhood"
	"strconv"
)

func main() {

	client := rh.NewClient()

	accs, err := client.GetAccounts()
	if err != nil {
		panic(err)
	}
	fmt.Println("Accounts:")
	for _, acc := range accs {
		fmt.Printf("Account: %v\n", acc.AccountNumber)
		client.AccountID = acc.AccountNumber
	}

	transfers, err := client.Transfers()
	if err != nil {
		panic(err)
	}
	fmt.Println("Transfer:")
	fmt.Println(transfers)

	total := 0.

	for _, transfer := range transfers {
		f, err := strconv.ParseFloat(transfer.Amount, 64)
		if err != nil {
			panic(err)
		}
		if strings.HasPrefix(transfer.PayableDate, "2019") {
			total = total + f
		}

	}

	fmt.Println("Transfer Total:")
	fmt.Println(total)

}
