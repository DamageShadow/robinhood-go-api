package robinhood

import (
	"encoding/json"
	"net/url"
	"time"
)

type WatchListItem struct {
	Watchlist  string    `json:"watchlist"`
	Instrument string    `json:"instrument"`
	CreatedAt  time.Time `json:"created_at"`
	URL        string    `json:"url"`
}

type EnhancedWatchListItem struct {
	//Watchlist  string
	//Instrument string
	Symbol    string
	Name      string
	CreatedAt time.Time
}

// Portfolio returns a slice of Position a user has in their account.
func (c *Client) WatchList() ([]EnhancedWatchListItem, error) {
	var enhancedWatchListItems []EnhancedWatchListItem
	wl, err := c.watchlist()
	if err != nil {
		return nil, err
	}
	for _, p := range wl {
		detail, err := getInstrument(p.Instrument)
		if err != nil {
			continue
		}
		enhancedWatchListItems = append(enhancedWatchListItems, EnhancedWatchListItem{
			Symbol:    detail.Symbol,
			Name:      detail.Name,
			CreatedAt: p.CreatedAt,
		})
	}
	return enhancedWatchListItems, nil
}

func (c *Client) watchlist() ([]WatchListItem, error) {
	params := url.Values{}
	params.Set("nonzero", "true")
	resp, err := c.paginatedGet(watchlistsURI + "Default/" + "?" + params.Encode())
	if err != nil {
		return nil, err
	}
	var watchList []WatchListItem
	err = json.Unmarshal(resp, &watchList)
	if err != nil {
		return nil, err
	}
	return watchList, nil
}
