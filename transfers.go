package robinhood

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"time"
)

type TransfersResponse struct {
	Previous string     `json:"previous"`
	Results  []Transfer `json:"results"`
	Next     string     `json:"next"`
}

type Transfer struct {
	Scheduled           bool        `json:"scheduled"`
	Direction           string      `json:"direction"`
	URL                 string      `json:"url"`
	RHSState            string      `json:"rhs_state"`
	CreatedAt           time.Time   `json:"created_at"`
	State               string      `json:"state"`
	RefID               string      `json:"ref_id"`
	UpdatedAt           time.Time   `json:"updated_at"`
	Amount              string      `json:"amount"`
	EarlyAccessAmount   string      `json:"early_access_amount"`
	Fees                string      `json:"fees"`
	Cancel              interface{} `json:"cancel"`
	AchRelationship     string      `json:"ach_relationship"`
	ExpectedLandingDate string      `json:"expected_landing_date"`
	StatusDescription   string      `json:"status_description"`
	ID                  string      `json:"id"`
}

type EnhancedTransfers struct {
	Symbol      string
	Name        string
	PayableDate string
	Amount      string
}

// Portfolio returns a slice of Position a user has in their account.
func (c *Client) Transfers() ([]EnhancedTransfers, error) {
	transfers, err := c.transfers()
	if err != nil {
		return nil, err
	}
	deposit := 0.0
	withdrawal := 0.0
	for _, t := range transfers {
		if t.Direction == "deposit" {
			amount, err := strconv.ParseFloat(t.Amount, 64)
			if err != nil {
				fmt.Printf("Error %s", err.Error())
			}
			deposit += amount
		}
		if t.Direction == "withdraw" {
			amount, err := strconv.ParseFloat(t.Amount, 64)
			if err != nil {
				fmt.Printf("Error %s", err.Error())
			}
			withdrawal += amount
		}
	}

	fmt.Printf("Deposited %f", deposit)
	fmt.Printf("Withdrawed %f", withdrawal)

	return nil, nil
}

func (c *Client) transfers() ([]Transfer, error) {
	params := url.Values{}
	params.Set("nonzero", "true")
	resp, err := c.get(transfersURI)
	if err != nil {
		return []Transfer{}, err
	}
	var transfersResponse TransfersResponse

	err = json.Unmarshal(resp, &transfersResponse)
	if err != nil {
		return []Transfer{}, err
	}
	return transfersResponse.Results, nil
}
